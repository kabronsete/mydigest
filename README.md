# MyDigest

This project is a gambas language component that provides a complete management of message digest

This code is a 1.0 version. Betatesters are welcome!!!

Suggestions are welcome

With this gambas class Your application will get an complete management of message digest.
- Function Checksum. This function creates a hash of a file, list os files or directories.
- Function StrChecksum. This function creates a hash of a string.
- Function SignFile. Sign the digest using a private key.
- Function VerifySign. Verify the signature using the public key

## Getting started

Add this component to your application and instantiate it is the only thing to do. Do it and spend your time on more productive things.

- Read the manual, see the examples and use it.
- Add the component to you application
- Instantiate it
	Dim f As New MyDigest, Response As String 
- Enjoy!!!

 

